package com.example.ufo.data;

import java.util.List;

import lombok.Data;

@Data
public class Ufos {
    private List<Ufo> ufos;
}
