package com.example.ufo.data;

public class Matrix {
    private final char[][] data;
    private final int width;
    private final int height;

    public Matrix(char[][] chars){
        data = chars;
        height = chars.length;
        width = chars[0].length;
    }
    public Matrix(String s){
        String[] split = s.split("\n");
        height= split.length;
        width =  split[0].length();

        data = new char[height][width];
        for (int row = 0; row < height; row++) {
            data[row] = split[row].toCharArray();
        }
    }

    public Matrix getSubMatrix(int x, int y, int w, int h){
        char[][] res = new char[h][w];
        for (int r = x; r < x+ w ; r++) {
            for (int c = y; c < y + h; c++) {
                res[c-y][r-x] = data[c][r];
            }
        }
        return new Matrix(res);
    }

    public char[][] getData() {
        return data;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }


}
