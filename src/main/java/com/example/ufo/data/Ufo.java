package com.example.ufo.data;

import lombok.Data;

@Data
public class Ufo {
    private String name;
    private String body;
}
