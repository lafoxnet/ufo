package com.example.ufo;

import java.util.List;

import com.example.ufo.data.MapServer;
import com.example.ufo.data.UfoResult;
import com.example.ufo.data.Ufos;

public interface UfoService {
    Ufos getUfos();
    MapServer getMap();

    List<UfoResult> findUfoOnMap();
}
