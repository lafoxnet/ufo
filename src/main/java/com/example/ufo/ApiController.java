package com.example.ufo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
public class ApiController {

    @Autowired
    private UfoService ufoService;

    @GetMapping("getUfos")
    public Object getUfos(){
        return ufoService.getUfos();
    }
    @GetMapping("getMap")
    public Object getMap(){
        return ufoService.getMap();
    }

    @GetMapping("findUfoOnMap")
    public Object findUfoOnMap(){
        return ufoService.findUfoOnMap();
    }
}
