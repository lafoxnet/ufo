package com.example.ufo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.ufo.data.MapServer;
import com.example.ufo.data.Matrix;
import com.example.ufo.data.Ufo;
import com.example.ufo.data.UfoResult;
import com.example.ufo.data.Ufos;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

@Service
public class UfoServiceImpl implements UfoService {

    private int limit =10;

    @Override
    public Ufos getUfos() {
        List<Ufo> ufoList = new ArrayList<>();
        Map<String, String> ufos = get("http://heti.lafox.net/api/getInvaders", HashMap.class);
        for (Map.Entry<String, String> entry : ufos.entrySet()) {
            Ufo ufo = new Ufo();
            ufo.setBody(entry.getValue());
            ufo.setName(entry.getKey());
            ufoList.add(ufo);
        }
        Ufos res = new Ufos();
        res.setUfos(ufoList);

        return res;

    }

    @Override
    public MapServer getMap() {
        return get("http://heti.lafox.net/api/getMap", MapServer.class);
    }

    @Override
    public List<UfoResult> findUfoOnMap() {
        MapServer mapObj = getMap();
        Matrix map = new Matrix(mapObj.getMap());
        Ufos ufos = getUfos();
        List<UfoResult> res = new ArrayList<>();
        for (Ufo ufoObj : ufos.getUfos()) {
            Matrix ufo = new Matrix(ufoObj.getBody());
            for (int x = 0; x < map.getWidth() - ufo.getWidth(); x++) {
                for (int y = 0; y < map.getHeight() - ufo.getHeight(); y++) {
                    Matrix subMatrix = map.getSubMatrix(x, y, ufo.getWidth(), ufo.getHeight());
                    int difCount = calculateDiffCount(ufo, subMatrix);
                    if (difCount < limit) {
                        UfoResult result = new UfoResult();
                        result.setDiffCount(difCount);
                        result.setX(x);
                        result.setY(y);
                        result.setName(ufoObj.getName());
                        res.add(result);
                    }
                }
            }
        }
        return res;
    }

    private int calculateDiffCount(Matrix ufo, Matrix subMatrix) {
        int res = 0;
        for (int x = 0; x < ufo.getWidth(); x++) {
            for (int y = 0; y < ufo.getHeight(); y++) {
                if (ufo.getData()[y][x] != subMatrix.getData()[y][x]){
                    res++;
                }
            }
        }
        return res;
    }

    private <T> T get(String url, Class<T> valueType) {
        ObjectMapper mapper = new ObjectMapper();
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url); ;// todo https://hc.apache.org/httpcomponents-client-4.5.x/quickstart.html
        try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
            HttpEntity entity = response.getEntity();
            String json = EntityUtils.toString(entity);
            return mapper.readValue(json, valueType);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
